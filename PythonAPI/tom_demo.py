'''
Created on Feb 17, 2016

@author: tom
'''
# %matplotlib inline
import matplotlib.pyplot as plt
from pycocotools.coco import COCO
from pycocotools.cocoeval import COCOeval
import numpy as np
import skimage.io as io
import pylab


if __name__ == '__main__':
    pylab.rcParams['figure.figsize'] = (10.0, 8.0)
    annType = ['segm','bbox']
    annType = annType[1]      #specify type here
    print 'Running demo for *%s* results.'%(annType)
    
    dataDir='../'
    my_dataDir='/home/tom/Downloads/COCO'
    
    #initialize COCO ground truth api
    dataType='val2014'
    annFile = '%s/annotations/instances_%s.json'%(my_dataDir,dataType)
    cocoGt=COCO(annFile)
    
    #initialize COCO detections api
    resFile='%s/results/instances_%s_fake%s100_results.json'
    resFile = resFile%(dataDir, dataType, annType)
    cocoDt=cocoGt.loadRes(resFile)    

    # visialuze gt and dt side by side
    imgIds=sorted(cocoGt.getImgIds())
    imgIds=imgIds[0:100]
    imgId = imgIds[np.random.randint(100)]
    img = cocoGt.loadImgs(imgId)[0]
#     I = io.imread('%s/images/val2014/%s'%(my_dataDir,img['file_name'])) 
    I = io.imread('%s/val2014/%s'%(my_dataDir,img['file_name'])) 
    
    # visialuze gt and dt side by side
    fig = plt.figure(figsize=[15,10])
    
    # ground truth
    plt.subplot(121)
    plt.imshow(I); plt.axis('off'); plt.title('ground truth')
    annIds = cocoGt.getAnnIds(imgIds=imgId)
    anns = cocoGt.loadAnns(annIds)
    cocoGt.showAnns(anns)
    
    # detections
    plt.subplot(122)
    plt.imshow(I); plt.axis('off'); plt.title('detections')
    annIds = cocoDt.getAnnIds(imgIds=imgId)
    anns = cocoDt.loadAnns(annIds)
    cocoDt.showAnns(anns)
    
    # running evaluation
    cocoEval = COCOeval(cocoGt,cocoDt)
    cocoEval.params.imgIds  = imgIds
    cocoEval.params.useSegm = (annType == 'segm')
    cocoEval.evaluate()
    cocoEval.accumulate()
    cocoEval.summarize()
    
    plt.show()

